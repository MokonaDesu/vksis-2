Component = require 'component'

# Represents monochannel with collision detection
class Channel extends Component
        constructor: (@outputHandler, @debugHandler) ->
                super
                @clearChannel()
                @currentOwner = null

        clearChannel: ->
                @transmitters = []
                @channelValue = 0
                               
        performChannelTick: ->
                if @transmitters.length > 1
                        t.onCollision() for t in @transmitters
                        @debugHandler.pushString "Collision detected!"
                else if @transmitters.length == 1
                        @currentOwner = @transmitters[0]
                        @nextElement.passValue @channelValue, this
                else
                        @currentOwner = null

        isReadyFor: (whom) -> @currentOwner == whom or not @currentOwner?
        
        tick: ->
                super
                @performChannelTick()
                @channelValue = 0
                @transmitters = []

module.exports = Channel
