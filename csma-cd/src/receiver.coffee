Component = require 'component'

# Represents basic data reciever
class Receiver extends Component
        tick: ->
                super
                @outputHandler.pushString if @channelValue?
                then "Receiver got byte: #{@channelValue}." else "No
                value this time around..." 
                @channelValue = null

module.exports = Receiver
