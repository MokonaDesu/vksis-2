Transmitter = require './transmitter'
Receiver = require './receiver'
Channel = require './channel'

Handler = require 'handler'

toBytes = (string) -> string.charCodeAt index for char, index in string

Handler.ready ->
        receiver = new Receiver(new Handler('reciever', '.recv'), null)

        channel = new Channel(null, new Handler('channel (debug)', '.dbg'))

        transmitter0 = new Transmitter(new Handler('first
        transmitter', '.tr-0'), null, 10,
        10, [1, 2, 3, 4, 5])
        
        transmitter1 = new Transmitter(new Handler('second
        transmitter', '.tr-1'), null, 10,
        10, [6, 7, 8, 9, 10])

        transmitter0.connectsTo(channel)
        transmitter1.connectsTo(channel)
        channel.connectsTo(receiver)

        Handler.event '.tick', 'click', (event) ->
                transmitter0.tick()
                transmitter1.tick()
                channel.tick()
                receiver.tick()

        Handler.event '.t0-send', 'click', (event) ->
                transmitter0.appendData(toBytes(Handler.controlValueAndClear('.tr-0-in')))

        Handler.event '.t1-send', 'click', (event) ->
                transmitter1.appendData(toBytes(Handler.controlValueAndClear('.tr-1-in')))
