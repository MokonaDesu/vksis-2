Component = require 'component'

# Represents transmitting device.
class Transmitter extends Component
        constructor: (outputHandler, debugHandler, @maxBackoff, @retries, @data) ->
                super outputHandler, debugHandler
                @dataQueue = []
                @backoff = 0
                @dataPosition = 0
                
        onCollision: ->
                @backoff = parseInt @maxBackoff * Math.random()
                @outputHandler.pushString "Initiate backoff: #{@backoff} ticks"
                @dataPosition = 0

        appendData: (data) ->
                @outputHandler.pushString "Enqueued message of #{data.length} bytes..." 
                if @data? then @dataQueue.push(data) else @data = data
                @done = no

        handleMsgEnd: ->
                @data = @dataQueue.shift()
                @dataPosition = 0
                if not @data?
                        @done = yes
                        @outputHandler.pushString "Done transmitting!
                        Total ticks: #{@totalTicks}" 
                else
                        @outputHandler.pushString "Done transmitting!
                        Giving up the channel for a bit..."
                        
        tick: ->
                super
                
                return if @done
                        
                if @dataPosition == @data.length
                        @handleMsgEnd()
                        return
                         
                if @backoff > 0
                        @outputHandler.pushString "#{@backoff--}
                        backoff ticks remain."
                else if not @nextElement.isReadyFor this
                        @outputHandler.pushString "Waiting for channel
                        to free..."
                else
                        nextValue = @data[@dataPosition++]
                        @outputHandler.pushString "Transmitting next
                        value: #{nextValue} on position
                        #{@dataPosition - 1}"
                        @nextElement.passValue nextValue, this


module.exports = Transmitter
