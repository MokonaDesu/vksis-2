$ = require 'jquery'

class Handler
        @ready: (fn) ->
                $(document).ready(fn)

        @event: (selector, type, event) ->
                $(selector).on(type, event)

        @controlValueAndClear: (selector) ->
                val = @controlValue(selector)
                $(selector).val('')
                val

        @controlValue: (selector) ->
                $(selector).val()

        @setOptions: (selector, values) ->
                for item in values then $(selector).append("<option value='#{item.key}'>#{item.val}</option>")

        @arbitrary: (selector, func) ->
                $(selector)[func].call($(selector))

        @append: (selector, what) ->
                $(selector).append(what)
                
        constructor: (@name, selector) ->
                @uiPart = $(selector)
                if not @uiPart.length
                        throw new Error 'No element found =('
                else
                        @pushString "Hello! This is #{@name}!"

        setClass: (cssClass) ->
                @uiPart.addClass cssClass

        removeClass: (cssClass) ->
                @uiPart.removeClass cssClass
                        
        pushString: (string) ->
                @uiPart.append("<li class=\"handler-entry\">#{string}</li>")
                @uiPart.scrollTop(Number.MAX_SAFE_INTEGER)

module.exports = Handler
