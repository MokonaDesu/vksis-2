# Represents base component in the network
class Component
        constructor: (@outputHandler, @debugHandler) ->
                @totalTicks = 0
                @transmitters = []
                
        passValue: (value, transmitter) ->
                @transmitters.push transmitter
                @channelValue |= value

        connectsTo: (element) ->
                @nextElement = element
                element

        onCollision: -> null

        isReadyFor: -> yes
                
        tick: -> @totalTicks++

module.exports = Component
