# [ StartDelimiter(omitted) - AccessControl - EndDelimiter(omitted) ]
class Packet
        constructor: (ac) ->
                @accessControl = { priority: ac.priority, token: ac.token, monitor: ac.monitor }

# [ StartDelimiter(omitted) - AccessControl - FrameControl(omitted) - DstAddress - SrcAddress - Data - CRC(omitted) - EndDelimiter(omitted) - FrameStatus ]
class DataPacket extends Packet
        constructor: (ac, @srcAddress, @dstAddress, @data) ->
                super ac
                @frameStatus =
                        addressRecognized: no
                        dataCopied: no
                        

module.exports.Packet = Packet
module.exports.DataPacket = DataPacket
