Station = require './station'

Handler = require 'handler'

formStationRing = (amount) ->
        stations = {}
        for num in [0..(amount - 1)]
                Handler.append '.main-body', "<ul class='st station-#{num}'></ul>"
                stations['.station-'+num] = new Station(new Handler('Station-'+num, '.station-'+num), null, '.station-'+num, num is 0)

        prevStation = null
        for station of stations
                if prevStation?
                        prevStation.connectsTo(stations[station])
                prevStation = stations[station]

        stations['.station-'+(amount-1)].connectsTo stations['.station-0']
        stations


Handler.ready ->
        stations = formStationRing 5
        srtStation = dstStation = null
        currentStation = stations['.station-0']

        selector = '.src-station, .dst-station'
        Handler.setOptions(selector, (for station of stations then { key: station, val: station }))
        Handler.setOptions('.dst-station', [ { key: 'other', val: 'Other' } ])
        
        Handler.event '.tick', 'click', (event) ->
                currentStation.tick()
                currentStation = currentStation.nextElement
                
        Handler.event '.dst-station', 'change', (event) ->
                Handler.arbitrary '.dst-address', if event.target.value is 'other' then 'show' else 'hide'
                
        Handler.event '.send', 'click', (event) ->
                srcStation = stations[Handler.controlValue('.src-station')]
                dstStation = Handler.controlValue('.dst-station')
                dstStation = if dstStation is 'other' then Handler.controlValueAndClear('.dst-address') else stations[dstStation].stationAddress
                priority = Number(Handler.controlValue '.priority-input')
                priority = 0 if priority isnt priority
                srcStation.enqueueMessage Handler.controlValueAndClear('.msg-value'), parseInt(priority), dstStation
