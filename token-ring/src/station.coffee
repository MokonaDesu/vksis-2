# Access control
# The data transmission process goes as follows:
#
# Empty information frames are continuously circulated on the ring.
#
# When a computer has a message to send, it seizes the token. The computer
# will then be able to send the frame.
#
# The frame is then examined by each successive workstation. The workstation
# that identifies itself to be the destination for the message copies it from
# the frame and changes the token back to 0.
# 
# When the frame gets back to the originator, it sees that the token has been
# changed to 0 and that the message has been copied and received. It removes
# the message from the frame.
# 
# The frame continues to circulate as an "empty" frame, ready to be taken by
# a workstation when it has a message to send.

Component = require 'component'
Packet = require('./packet').Packet
DataPacket = require('./packet').DataPacket


class Station extends Component
        constructor: (outputHandler, debugHandler, @stationAddress, @isMonitor) ->
                super outputHandler, debugHandler
                @msgQueue = []
                @passTokenOnNextTick = no
                if @isMonitor
                        @outputHandler.setClass 'got-token'
                        @outputHandler.setClass 'next-tick'
                        @outputHandler.setClass 'monitor-station'
                        @token = new Packet priority: 0, token: yes, monitor: no

        passData: (dataPacket) ->
                if dataPacket.accessControl.token
                        @outputHandler.setClass 'got-token'
                        @outputHandler.pushString "Got token packet with priority of #{dataPacket.accessControl.priority}"
                        @token = dataPacket
                else
                        @packetToPass = dataPacket
                        @handleIncoming dataPacket

        handleIncoming: (dataPacket) ->
                @processRecievedMsg dataPacket if dataPacket.dstAddress is @stationAddress
                @processSentMsg dataPacket if dataPacket.srcAddress is @stationAddress
                @monitorPacket dataPacket if @isMonitor

        monitorPacket: (dataPacket) ->
                if dataPacket.accessControl.monitor
                        @drainPacket()
                else
                        dataPacket.accessControl.monitor = yes

        processRecievedMsg: (dataPacket) ->
                dataPacket.frameStatus.addressRecognized = yes
                @copyMessage dataPacket
                @outputHandler.pushString "Recieved msg (#{dataPacket.data.length} bytes) from #{dataPacket.srcAddress}."

        drainPacket: (dataPacket) ->
                @packetToPass = null
                
        processSentMsg: (dataPacket) ->
                @drainPacket()
                @passTokenOnTick()
                if dataPacket.frameStatus.addressRecognized
                        @outputHandler.pushString "Draining packet acknowledged by destination station #{dataPacket.dstAddress}."
                else
                        @outputHandler.pushString "Draining stray packet. Too bad that #{dataPacket.dstAddress} was not around to recieve it =("

        passTokenOnTick: ->
                @passTokenOnNextTick = yes

        passToken: ->
                @outputHandler.removeClass 'got-token'
                @packetToPass = @token
                @token = null
                @passPacketBy()
                        
        copyMessage: (dataPacket) ->
                # portray serious activity here
                # and when done...
                dataPacket.frameStatus.dataCopied = yes

        enqueueMessage: (data, priority, target) ->
                priority = Math.min(Math.max(priority, 0), 7)
                @msgQueue.push new DataPacket { priority, token: no, monitor: @isMonitor }, @stationAddress, target, data
                @msgQueue.sort (msg0, msg1) -> msg0.accessControl.priority > msg1.accessControl.priority
                @outputHandler.pushString "Packet enqueued with priority of #{priority}."

        passPacketBy: ->
                @nextElement.passData @packetToPass
                @packetToPass = null

        sendOwnPacket: ->
                msg = @msgQueue.shift()
                if msg?
                        @outputHandler.pushString "Sending my own packet"
                        @packetToPass = msg
                        @passPacketBy()
                else
                        @outputHandler.pushString "Got nothing to send so just giving the token away"
                        @passToken()
                
        tick: ->
                @outputHandler.removeClass 'next-tick'
                @nextElement.outputHandler.setClass 'next-tick'

                if @passTokenOnNextTick
                        @passTokenOnNextTick = no
                        @outputHandler.pushString "Just acknowledged the packet, giving up the token."
                        return @passToken()
                        
                if @packetToPass?
                        @outputHandler.pushString "Let the packet fly by..."
                        @passPacketBy()
                        
                else if @token?        
                        @sendOwnPacket()
                                
                else @outputHandler.pushString "Didn't do anything =("


module.exports = Station
        
